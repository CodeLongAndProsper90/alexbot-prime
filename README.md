# AlexBot Prime
From the original readme:
> \# AlexBot
> My Discord bot.
> This is my discord bot that I have been making. It performs many functions(mostly for fun). 
> Thank You for checking out this repo.

After the original maintainer abandoned it, citing reasons of feature creep, I have taken the mantle.

## Commands:
Command name | Discord command | function
------------ | --------------- | --------
Alive?       | `lol alive?`    | Basic status checker
Help!        | `lol helpme`    | Provides help (website is offline) 
/dev/null    | `lol nothing`   | Does nothing. Garbage in, garbage out
Latency      | `lol latency`   | Gets the bot's latency
Spam ping    | `lol spaming [username] [times]` | Pings the given user the given number of times (limit 50)
E            | `lol e`         | e
Pig Latin    | `lol piglatin [text]` | Translates given text into pig latin
ISS          | `lol iss`       | Gets location of the Internation Space Station
Number info  | `lol numberinfo` | Gets the info of a number?
GitHub       | `lol github [repo]` | Gets the GitHub info for the given repo
BitCoin      | `lol bitcoin`   | Gets current price of bitcoin
Load         | `lol load`      | This does nothing except show a loading bar. (:stare:)
Info         | `lol info`      | Shows info of the bot
Kahoot bomb  | `lol botkahoot [code]` | Nukes kahoot with bots. This is a possible ToS violation and may be __REMOVED__
Profile picture | `lol pp [user]` | Gets user's PFP. Name will change.

Undocumented:
    - broadcast
    - sboardcast
    - getmsg
    - msgquery

## FAQ:
Only one right now:
- Why is the discord bot named `Alexbot'`? What does that mean? Why no closing?
Well, in geometry, a transformed shape (`X`) becomes "X prime"— notated as `X'`. 
Since "Alexbot Prime" is clunky, "Alexbot'" is simpler and easier to remember
